import 'dart:io';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:webview_flutter/webview_flutter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pixo Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

enum ImageSourceType { gallery }

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State {
  var _image;
  var imagePicker;
  var _base64Image = '';
  var _webViewController;

  int _stackToView = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Pixo Flutter Example"),
      ),
      body: IndexedStack(
        index: _stackToView,
        children: [
          MaterialButton(
            color: Colors.blue,
            child: Text(
              "Pick Image from Gallery",
              style:
                  TextStyle(color: Colors.white70, fontWeight: FontWeight.bold),
            ),
            onPressed: () async {
              imagePicker = ImagePicker();
              var source = ImageSource.gallery;
              XFile image = await imagePicker.pickImage(
                  source: source,
                  imageQuality: 50,
                  preferredCameraDevice: CameraDevice.front);

              final bytes = File(image.path).readAsBytesSync();
              _base64Image = base64Encode(bytes);

              setState(() {
                _stackToView = 1;
              });

              _webViewController.evaluateJavascript(
                  'EditImageWithPixo("data:image/png;base64,$_base64Image")');
            },
          ),
          WebView(
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController controller) {
              _webViewController = controller;
            },
            javascriptChannels: Set.from([
              JavascriptChannel(
                  name: 'JavascriptChannel',
                  onMessageReceived: (JavascriptMessage message) {
                    //This is where you receive message from
                    //javascript code and handle in Flutter/Dart
                    //like here, the message is just being printed
                    //in Run/LogCat window of android studio
                    setState(() {
                      _base64Image = message.message;
                      _stackToView = _base64Image != '' ? 2 : 0;
                    });
                  })
            ]),
            initialUrl: Uri.dataFromString("""
                    <html>
                    <head>
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    </head>
                    <body>
                        <script src="https://pixoeditor.com/editor/scripts/bridge.m.js"></script>
                        <script type="text/javascript">
                        function EditImageWithPixo(img) {
                          new Pixo.Bridge({
                              type: 'modal',
                              apikey: '26uqyj79pdgk',
                              onSave: image => JavascriptChannel.postMessage(image.toBase64()),
                              onCancel: () => JavascriptChannel.postMessage(''),
                          }).edit(img)
                        }
                        </script>
                    </body>
                    </html>
                    """, mimeType: 'text/html').toString(),
          ),
          _base64Image != ''
              ? Image.memory(base64Decode(_base64Image))
              : Text('No image'),
        ],
      ),
    );
  }
}
